using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowEffect : MonoBehaviour
{
    [SerializeField] private float _effectDestroyTime;
    void Start()
    {
        Invoke("DestroyEffect", _effectDestroyTime);
    }
    private void DestroyEffect()
    {
        Destroy(gameObject);
    }
}
