using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerHealthView : MonoBehaviour
{
    [SerializeField] private PlayerHealthController _healthController;
    [SerializeField] private TMP_Text _healthText;
    void Update()
    {
        _healthText.text = "Health: " + _healthController.HP;
    }
}
