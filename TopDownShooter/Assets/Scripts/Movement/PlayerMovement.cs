using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private float _speed = 1;
    void FixedUpdate()
    {
        MovePlayer(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }
    public void MovePlayer(float x, float y)
    {
        _rigidbody2D.MovePosition(_rigidbody2D.position + new Vector2(x, y) * _speed * Time.fixedDeltaTime);
    }
}
