using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotator : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    private Vector3 _direction;
    private Vector3 _moveTo;
    
    void Update()
    {
        _moveTo = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _direction = _moveTo - transform.position;
        _direction.Normalize();
        LookAtPoint();
    }
    private void LookAtPoint()
    {
        float angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
        _rigidbody2D.rotation = angle;
    }
}
